module test();

reg clk;
reg reset;

reg start;
reg step2;
reg step3;
wire [2:0]fsm_out;

always #20 clk =~clk;


FSM FSM0(
.clk(clk),
.reset(reset),
.start(start),
.step2(step2),
.step3(step3),
.fsm_out(fsm_out)
);


initial begin
clk = 0;
reset = 0;
start = 0;
step2 = 0;
step3 = 0;
#100;
reset = 1;
start = 1;
#10;
//start = 0;
#10;
step2 = 1;
#10;
//step2 = 0;
#10;
step3 = 1;
#10;
//step3 = 0;
#100;
$display("the end.\n");

$finish;

end
endmodule
